#include "association.h"

#include <iostream>
#include <string>

#include <windows.h>

#include "md5.h"
#include "ms_hash.h"
#include "win32api.h"

const std::wstring kHashStringSuffix =
    L"user choice set via windows user experience "
    L"{d18b6dd5-6124-4341-9318-804003bafa0b}";


bool SetAssociation(std::wstring protocol, std::wstring progid) {
  std::wstring key_path =
    L"Software\\Microsoft\\Windows\\Shell\\Associations\\UrlAssociations\\"
    + protocol
    + L"\\UserChoice";

  std::wstring user_sid = GetUserSID();
  std::wstring key_last_write_time = CreateAndGetRegKeyTimestamp(key_path);

  std::wstring to_be_hashed = protocol + user_sid + progid +
                              key_last_write_time + kHashStringSuffix;
  wchar_t tmp[10240];
  to_be_hashed.copy(tmp, 10240);
  tmp[to_be_hashed.size()] = 0;
  _wcslwr_s(tmp, to_be_hashed.size() + 1);
  std::wstring to_be_hashed_lower(tmp);

  std::wcout << to_be_hashed_lower << std::endl;

  unsigned char md5_hash[kMd5OutputSize];
  md5(reinterpret_cast<const unsigned char*>(to_be_hashed_lower.c_str()),
      (to_be_hashed.size() + 1) * sizeof(std::wstring::traits_type::char_type),
      md5_hash);

  int wordswap_out[2], reversible_out[2];
  unsigned int cs64_size =
      (to_be_hashed.size() * sizeof(std::wstring::traits_type::char_type)) /
      sizeof(int);
  cs64_size = (cs64_size - 1) + (!(cs64_size % 2));

  CS64_WordSwap(
      reinterpret_cast<const int*>(to_be_hashed_lower.c_str()),
      cs64_size,
      reinterpret_cast<const unsigned int*>(md5_hash),
      wordswap_out);
  CS64_Reversible(
      reinterpret_cast<const int*>(to_be_hashed_lower.c_str()),
      cs64_size,
      reinterpret_cast<const unsigned int*>(md5_hash),
      reversible_out);

  int xored_out[2] = {
      wordswap_out[0] ^ reversible_out[0],
      wordswap_out[1] ^ reversible_out[1]
  };

  std::wstring base64_hash = Base64Encode(
      reinterpret_cast<const unsigned char*>(xored_out),
      sizeof(xored_out));

  SetRegValue(key_path, L"Hash", base64_hash);
  SetRegValue(key_path, L"ProgId", progid);

  return true;
}
