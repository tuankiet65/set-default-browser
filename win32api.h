#ifndef WIN32API_H
#define WIN32API_H

#include <string>

std::wstring GetUserSID();
std::wstring CreateAndGetRegKeyTimestamp(std::wstring key_path);
std::wstring Base64Encode(const unsigned char data[],
                          const unsigned int data_size);
bool SetRegValue(std::wstring key,
                 std::wstring value_name, std::wstring value_data);
void NotifyWindowsShell();

#endif
