#include "md5.h"

#include <iostream>
#include <memory>

#include <windows.h>
#include <bcrypt.h>

#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0)

bool md5(const unsigned char data[], const unsigned int data_len,
         unsigned char output[kMd5OutputSize]) {
  BCRYPT_ALG_HANDLE handle;
  if (!NT_SUCCESS(BCryptOpenAlgorithmProvider(&handle,
                                              BCRYPT_MD5_ALGORITHM,
                                              nullptr,
                                              0))) {
    return false;
  }

  DWORD hash_object_size, hash_object_actual_size;
  if (!NT_SUCCESS(BCryptGetProperty(handle, BCRYPT_OBJECT_LENGTH,
                                    reinterpret_cast<PUCHAR>(&hash_object_size),
                                    sizeof(hash_object_size),
                                    &hash_object_actual_size, 0))) {
    return false;
  }

  if (sizeof(hash_object_size) != hash_object_actual_size) {
    return false;
  }

  std::unique_ptr<unsigned char[]> hash_object(
    new unsigned char[hash_object_size]);
  BCRYPT_HASH_HANDLE hash_handle;
  if (!NT_SUCCESS(BCryptCreateHash(handle, &hash_handle,
                                   hash_object.get(), hash_object_size,
                                   nullptr, 0, 0))) {
    return false;
  }

  if (!NT_SUCCESS(BCryptHashData(hash_handle,
                                 const_cast<PUCHAR>(data), data_len,
                                 0))) {
    return false;
  }

  DWORD hash_output_size, hash_output_output_size;
  if (!NT_SUCCESS(BCryptGetProperty(hash_handle,
                                    BCRYPT_HASH_LENGTH,
                                    reinterpret_cast<PUCHAR>(&hash_output_size),
                                    sizeof(hash_output_size),
                                    &hash_output_output_size, 0))) {
    return false;
  }

  if (hash_output_size != kMd5OutputSize) {
    return false;
  }

  if (!NT_SUCCESS(BCryptFinishHash(hash_handle, output, kMd5OutputSize, 0))) {
    return false;
  }

  BCryptDestroyHash(hash_handle);
  BCryptCloseAlgorithmProvider(handle, 0);

  return true;
}

