#ifndef MS_HASH_H
#define MS_HASH_H

void CS64_WordSwap(const int data[], const unsigned int data_length,
                   const unsigned int md5_hash[], int out[2]);

void CS64_Reversible(const int data[], const unsigned int data_length,
                     const unsigned int md5_hash[], int out[2]);

#endif
