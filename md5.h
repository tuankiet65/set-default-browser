#ifndef MD5_H
#define MD5_H

const size_t kMd5OutputSize = 16;

bool md5(const unsigned char data[], const unsigned int data_length,
         unsigned char md5[16]);

#endif MD5_H
