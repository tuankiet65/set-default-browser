#include "win32api.h"

#include <iostream>
#include <string>

#include <atlstr.h>
#include <sddl.h>
#include <shlobj_core.h>
#include <windows.h>
#include <wincrypt.h>

std::wstring GetUserSID() {
  HANDLE process_token;
  if (!OpenProcessToken(GetCurrentProcess(), GENERIC_READ, &process_token)) {
    std::cerr << "Unable to get current process token";
    DWORD error_code = GetLastError();
    std::cerr << "Error code: " << error_code;
    return std::wstring();
  }

  // First pass to get buffer size
  DWORD token_user_size = 256;
  std::unique_ptr<TOKEN_USER> token_user(
      reinterpret_cast<TOKEN_USER*>(new BYTE[token_user_size]));
  if (!GetTokenInformation(process_token, TokenUser, token_user.get(),
                           token_user_size, &token_user_size)) {
    DWORD error_code = GetLastError();
    if (error_code != ERROR_INSUFFICIENT_BUFFER) {
      std::cerr << "Error while calling GetTokenInformation: " << error_code;
      return std::wstring();
    }

    // resize the buffer
    token_user.reset(reinterpret_cast<TOKEN_USER*>(new BYTE[token_user_size]));
  }

  if (!GetTokenInformation(process_token, TokenUser, token_user.get(),
                           token_user_size, &token_user_size)) {
    std::cerr << "Unable to retrieve user token information: "
              << GetLastError();
    return std::wstring();
  }

  // https://stackoverflow.com/a/15067510
  LPWSTR temp_lpwstr;
  if (ConvertSidToStringSidW((token_user.get()->User.Sid),
                              &temp_lpwstr) == 0) {
    std::cerr << "Unable to convert SID to string";
    return std::wstring();
  }

  std::wstring result(temp_lpwstr);
  LocalFree(temp_lpwstr);

  return result;
}

std::wstring CreateAndGetRegKeyTimestamp(std::wstring key_path) {
  if (!RegDeleteKeyEx(HKEY_CURRENT_USER, key_path.c_str(), KEY_WOW64_32KEY,
                      0) != ERROR_SUCCESS) {
    // TODO: maybe a warning
  }

  HKEY key_handle;
  if (RegCreateKeyEx(HKEY_CURRENT_USER, key_path.c_str(), 0, nullptr, 0,
                     KEY_WRITE | KEY_QUERY_VALUE, nullptr,
                     &key_handle, NULL) != ERROR_SUCCESS) {
    return std::wstring();
  }

  // We're only interested in the last write time
  FILETIME key_last_write_time;
  if (RegQueryInfoKey(key_handle, nullptr, nullptr, nullptr, nullptr, nullptr,
                      nullptr, nullptr, nullptr, nullptr, nullptr,
                      &key_last_write_time) != ERROR_SUCCESS) {
    std::cout << GetLastError();
    RegCloseKey(key_handle);
    return std::wstring();
  }

  SYSTEMTIME key_last_write_systemtime;
  if (!FileTimeToSystemTime(&key_last_write_time, &key_last_write_systemtime)) {
    RegCloseKey(key_handle);
    return std::wstring();
  }

  key_last_write_systemtime.wSecond = 0;
  key_last_write_systemtime.wMilliseconds = 0;

  if (!SystemTimeToFileTime(&key_last_write_systemtime, &key_last_write_time)) {
    RegCloseKey(key_handle);
    return std::wstring();
  }

  RegCloseKey(key_handle);

  wchar_t tmp[64] = {0};
  int length = swprintf_s(tmp, (sizeof(tmp) / sizeof(*tmp)),
                          L"%08x%08x",
                          key_last_write_time.dwHighDateTime,
                          key_last_write_time.dwLowDateTime);
  return std::wstring(tmp, length);
}

std::wstring Base64Encode(const unsigned char data[],
                          const unsigned int data_size) {
  DWORD required_size;
  if (!CryptBinaryToString(data, data_size,
                           CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF,
                           nullptr, &required_size)) {
    return std::wstring();
  }

  std::unique_ptr<wchar_t> buffer(new wchar_t[required_size]);
  if (!CryptBinaryToString(data, data_size,
                           CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF,
                           buffer.get(), &required_size)) {
    return std::wstring();
  }

  return std::wstring(buffer.get());
}

bool SetRegValue(std::wstring key,
                 std::wstring value_name, std::wstring value_data) {
  HKEY handle;
  if (RegOpenKeyEx(HKEY_CURRENT_USER, key.c_str(), 0, KEY_WRITE,
                   &handle) != ERROR_SUCCESS) {
    return false;
  }

  if (RegSetValueEx(handle, value_name.c_str(), 0, REG_SZ,
                    reinterpret_cast<const BYTE*>(value_data.c_str()),
                    (value_data.size() + 1) * sizeof(wchar_t)) != ERROR_SUCCESS) {
    return false;
  }

  RegCloseKey(handle);

  return true;
}

void NotifyWindowsShell() {
  SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, 0, 0);
}
