#include "ms_hash.h"

// FUN_00401fc0
void CS64_WordSwap(const int data[], const unsigned int data_length,
                   const unsigned int md5_hash[], int out[2]) {
  unsigned int uVar1;
  int iVar2;
  int iVar3;
  unsigned int uVar4;
  int iVar5;
  const int *piVar6;
  int iVar7;
  int iVar8;
  int iVar9;
  int iVar10;

  if ((1 < data_length) && ((data_length & 1) == 0)) {
    iVar9 = 0;
    uVar1 = data_length - 2 >> 1;
    iVar2 = uVar1 + 1;
    iVar3 = 0;
    iVar10 = (md5_hash[1] | 1) + 0x13db0000;
    iVar8 = (md5_hash[0] | 1) + 0x69fb0000;
    piVar6 = data;
    iVar7 = iVar2;
    do {
      uVar4 = (iVar3 + *piVar6) * iVar8 + ((unsigned int)(iVar3 + *piVar6) >> 0x10) * -0x10fa9605;
      uVar4 = (uVar4 >> 0x10) * 0x689b6b9f + uVar4 * 0x79f8a395;
      iVar5 = uVar4 * -0x1568ffff + (uVar4 >> 0x10) * -0x3c101569;
      uVar4 = iVar5 + piVar6[1];
      uVar4 = uVar4 * iVar10 + (uVar4 >> 0x10) * -0x3ce8ec25;
      uVar4 = uVar4 * 0x59c3af2d + (uVar4 >> 0x10) * -0x2232e0f1;
      iVar3 = (uVar4 >> 0x10) * 0x35bd1ec9 + uVar4 * 0x1ec90001;
      iVar9 = iVar9 + iVar5 + iVar3;
      iVar7 = iVar7 + -1;
      piVar6 = piVar6 + 2;
    } while (iVar7 != 0);
    if ((data_length - 2) + uVar1 * -2 == 1) {
      uVar1 = iVar8 * (iVar3 + data[iVar2 * 2]) +
              ((unsigned int)(iVar3 + data[iVar2 * 2]) >> 0x10) * -0x10fa9605;
      uVar1 = uVar1 * 0x28dba395 + (uVar1 >> 0x10) * 0x39646b9f +
              (uVar1 * 0x79f8a395 + (uVar1 >> 0x10) * 0x689b6b9f >> 0x10) * -0x3c101569;
      uVar4 = iVar10 * uVar1 + (uVar1 >> 0x10) * -0x3ce8ec25;
      iVar3 = uVar4 * 0x2a18af2d + (uVar4 >> 0x10) * 0x2941f0f +
              (uVar4 * 0x59c3af2d + (uVar4 >> 0x10) * -0x2232e0f1 >> 0x10) * 0x35bd1ec9;
      iVar9 = iVar9 + uVar1 + iVar3;
    }

    out[0] = iVar3;
    out[1] = iVar9;
  }
}

// FUN_00402140
void CS64_Reversible(const int data[], const unsigned int data_length,
                     const unsigned int md5_hash[], int out[2]) {
  unsigned int uVar1;
  int iVar2;
  const int *piVar3;
  int iVar4;
  unsigned int uVar5;
  unsigned int uVar6;
  unsigned int uVar7;
  unsigned int uVar8;
  int iVar9;
  int iVar10;
  int iVar11;

  if ((1 < data_length) && ((data_length & 1) == 0)) {
    iVar4 = 0;
    iVar10 = 0;
    uVar1 = data_length - 2 >> 1;
    iVar2 = uVar1 + 1;
    uVar7 = *md5_hash | 1;
    uVar8 = md5_hash[1] | 1;
    piVar3 = data;
    iVar9 = iVar2;
    do {
      uVar5 = (iVar4 + *piVar3) * uVar7 >> 0x10;
      uVar6 = uVar7 * -0x4eef0000 * (iVar4 + *piVar3) + uVar5 * -0x30674eef >> 0x10;
      uVar5 = uVar5 * 0x48f0000 + uVar6 * -0x78f7a461 >> 0x10;
      iVar11 = (uVar5 * 0x12ceb96d + uVar6 * -0x164d0000 >> 0x10) * 0x257e1d83 + uVar5 * 0x3bc70000;
      iVar4 = piVar3[1] + iVar11;
      uVar5 = iVar4 * uVar8 >> 0x10;
      uVar6 = uVar8 * 0x16f50000 * iVar4 + uVar5 * -0x5d8be90b >> 0x10;
      uVar5 = uVar5 * 0x6c0b0000 + uVar6 * -0x2c7c6901 >> 0x10;
      iVar4 = uVar5 * -0xdcf0000 + (uVar5 * 0x7c932b89 + uVar6 * -0x5c890000 >> 0x10) * -0x405b6097;
      iVar10 = iVar10 + iVar11 + iVar4;
      iVar9 = iVar9 + -1;
      piVar3 = piVar3 + 2;
    } while (iVar9 != 0);
    if ((data_length - 2) + uVar1 * -2 == 1) {
      uVar7 = (iVar4 + data[iVar2 * 2]) * uVar7;
      uVar5 = uVar7 >> 0x10;
      uVar1 = uVar7 * -0x4eef0000 + uVar5 * -0x30674eef >> 0x10;
      uVar7 = uVar5 * 0x48f0000 + uVar1 * -0x78f7a461 >> 0x10;
      iVar9 = (uVar7 * 0x12ceb96d + uVar1 * -0x164d0000 >> 0x10) * 0x257e1d83 + uVar7 * 0x3bc70000;
      uVar8 = uVar8 * iVar9;
      uVar7 = uVar8 >> 0x10;
      uVar8 = uVar8 * 0x16f50000 + uVar7 * -0x5d8be90b >> 0x10;
      uVar7 = uVar7 * 0x6c0b0000 + uVar8 * -0x2c7c6901 >> 0x10;
      iVar4 = uVar7 * -0xdcf0000 + (uVar7 * 0x7c932b89 + uVar8 * -0x5c890000 >> 0x10) * -0x405b6097;
      iVar10 = iVar10 + iVar9 + iVar4;
    }

    out[0] = iVar4;
    out[1] = iVar10;
  }
}
