#include <iostream>
#include <string>

#include "association.h"
#include "md5.h"
#include "win32api.h"

int main(int argc, const char** argv) {
  std::string progid(argv[1]);
  // Let's hope progid isn't a multibyte string (only contains ASCII characters)
  std::wstring progid_wstring(progid.begin(), progid.end());

  SetAssociation(L"http", progid_wstring);
  SetAssociation(L"https", progid_wstring);

  NotifyWindowsShell();

  return 0;
}
